<?php
declare(strict_types=1);
namespace Thunder\Linkz\Link\Domain\Entity;

use Ramsey\Uuid\UuidInterface;

final class Link
{
    private $id;
    private $alias;
    private $url;
    private $createdAt;

    public function __construct(UuidInterface $id, string $alias, string $url, \DateTimeImmutable $createdAt)
    {
        $this->id = $id;
        $this->alias = $alias;
        $this->url = $url;
        $this->createdAt = $createdAt;
    }

    public function getId(): UuidInterface
    {
        return $this->id;
    }

    public function getAlias(): string
    {
        return $this->alias;
    }

    public function getUrl(): string
    {
        return $this->url;
    }

    public function getCreatedAt(): \DateTimeImmutable
    {
        return $this->createdAt;
    }
}
