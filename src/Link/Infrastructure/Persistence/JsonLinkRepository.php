<?php
declare(strict_types=1);
namespace Thunder\Linkz\Link\Infrastructure\Persistence;

use Ramsey\Uuid\Uuid;
use Thunder\Linkz\JsonFile;
use Thunder\Linkz\Link\Domain\Entity\Link;
use Thunder\Linkz\Exception\LinkNotFoundException;

final class JsonLinkRepository implements LinkRepositoryInterface
{
    private $json;

    public function __construct(JsonFile $json)
    {
        $this->json = $json;
    }

    public function persist(Link $link): void
    {
        $this->json->apply(function(array &$data) use($link) {
            $data['link'][$link->getAlias()] = [
                'id' => $link->getId()->toString(),
                'alias' => $link->getAlias(),
                'url' => $link->getUrl(),
                'createdAt' => $link->getCreatedAt()->format(\DATE_ATOM),
            ];
        });
    }

    public function findLastN(int $n): array
    {
        return $this->json->reduce(function(array $data) {
            $data = $data['link'] ?: [];
            usort($data, function(array $lhs, array $rhs) {
                return $rhs['createdAt'] <=> $lhs['createdAt'];
            });

            return array_map(function(array $item) {
                return new Link(Uuid::fromString($item['id']), $item['alias'], $item['url'], new \DateTimeImmutable($item['createdAt']));
            }, \array_slice($data, 0, 10));
        });
    }

    public function findByAlias(string $alias): Link
    {
        return $this->json->reduce(function(array $data) use($alias) {
            if(false === array_key_exists($alias, $data['link'] ?? [])) {
                throw new LinkNotFoundException(sprintf('There is no URL with alias `%s`.', $alias));
            }

            $item = $data['link'][$alias];

            return new Link(Uuid::fromString($item['id']), $item['alias'], $item['url'], new \DateTimeImmutable($item['createdAt']));
        });
    }

    public function findByUrl(string $url): Link
    {
        return $this->json->reduce(function(array $data) use($url) {
            foreach($data['link'] ?? [] as $id => $item) {
                if($url === $item['url']) {
                    return new Link(Uuid::fromString($item['id']), $item['alias'], $item['url'], new \DateTimeImmutable($item['createdAt']));
                }
            }

            throw new LinkNotFoundException(sprintf('There is no link with URL `%s`.', $url));
        });
    }
}
