<?php
declare(strict_types=1);
namespace Thunder\Linkz\Link\Infrastructure\Persistence;

use Thunder\Linkz\Link\Domain\Entity\Link;

interface LinkRepositoryInterface
{
    public function persist(Link $link): void;

    /**
     * @param int $n
     *
     * @return Link[]
     */
    public function findLastN(int $n): array;

    public function findByAlias(string $alias): Link;

    public function findByUrl(string $url): Link;
}
