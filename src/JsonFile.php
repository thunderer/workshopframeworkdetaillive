<?php
declare(strict_types=1);
namespace Thunder\Linkz;

final class JsonFile
{
    private $path;
    private $data;

    public function __construct(string $path)
    {
        $this->path = $path;
        $this->refresh();
    }

    public function apply(callable $handler): void
    {
        $this->load();
        $handler($this->data);
    }

    public function reduce(callable $handler)
    {
        $this->load();

        return $handler($this->data);
    }

    private function load(): void
    {
        if(null === $this->data) {
            $this->refresh();
        }
    }

    public function refresh(): void
    {
        if(false === file_exists($this->path)) {
            throw new \RuntimeException(sprintf('JSON file not found `%s`.', $this->path));
        }

        $this->data = json_decode(file_get_contents($this->path), true);
    }

    public function flush(): void
    {
        file_put_contents($this->path, json_encode($this->data, JSON_PRETTY_PRINT));
    }

    public function reset(): void
    {
        $this->data = [];
    }
}
