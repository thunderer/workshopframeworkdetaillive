<?php
declare(strict_types=1);
namespace Thunder\Linkz\Tests\Unit\Link\Domain\Entity;

use PHPUnit\Framework\TestCase;
use Ramsey\Uuid\Uuid;
use Thunder\Linkz\Link\Domain\Entity\Link;

final class LinkTest extends TestCase
{
    public function testLink(): void
    {
        $link = new Link(Uuid::uuid4(), 'alias', 'http://example.com', new \DateTimeImmutable());

        $this->assertSame('alias', $link->getAlias());
    }
}
