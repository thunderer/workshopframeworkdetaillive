<?php

use Symfony\Component\DependencyInjection\Argument\RewindableGenerator;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\DependencyInjection\Exception\InvalidArgumentException;
use Symfony\Component\DependencyInjection\Exception\LogicException;
use Symfony\Component\DependencyInjection\Exception\RuntimeException;
use Symfony\Component\DependencyInjection\ParameterBag\FrozenParameterBag;

/**
 * This class has been auto-generated
 * by the Symfony Dependency Injection Component.
 *
 * @final since Symfony 3.3
 */
class LinkzContainer extends Container
{
    private $parameters;
    private $targetDirs = array();

    /**
     * @internal but protected for BC on cache:clear
     */
    protected $privates = array();

    public function __construct()
    {
        $this->services = $this->privates = array();
        $this->methodMap = array(
            'index.controller' => 'getIndex_ControllerService',
            'json.file' => 'getJson_FileService',
            'link.controller' => 'getLink_ControllerService',
            'link.repository' => 'getLink_RepositoryService',
            'router' => 'getRouterService',
        );

        $this->aliases = array();
    }

    public function reset()
    {
        $this->privates = array();
        parent::reset();
    }

    public function compile()
    {
        throw new LogicException('You cannot compile a dumped container that was already compiled.');
    }

    public function isCompiled()
    {
        return true;
    }

    public function getRemovedIds()
    {
        return array(
            'Psr\\Container\\ContainerInterface' => true,
            'Symfony\\Component\\DependencyInjection\\ContainerInterface' => true,
            'link.repository.json' => true,
            'router.loader' => true,
            'router.locator' => true,
            'twig' => true,
            'twig.loader' => true,
        );
    }

    /**
     * Gets the public 'index.controller' shared service.
     *
     * @return \Thunder\Linkz\Application\Linkz\Controller\IndexController
     */
    protected function getIndex_ControllerService()
    {
        return $this->services['index.controller'] = new \Thunder\Linkz\Application\Linkz\Controller\IndexController(($this->services['router'] ?? $this->getRouterService()), ($this->privates['twig'] ?? $this->getTwigService()));
    }

    /**
     * Gets the public 'json.file' shared service.
     *
     * @return \Thunder\Linkz\JsonFile
     */
    protected function getJson_FileService()
    {
        return $this->services['json.file'] = new \Thunder\Linkz\JsonFile('../var/cache/db.json');
    }

    /**
     * Gets the public 'link.controller' shared service.
     *
     * @return \Thunder\Linkz\Application\Linkz\Controller\LinkController
     */
    protected function getLink_ControllerService()
    {
        return $this->services['link.controller'] = new \Thunder\Linkz\Application\Linkz\Controller\LinkController(($this->services['router'] ?? $this->getRouterService()), ($this->services['link.repository'] ?? $this->getLink_RepositoryService()), ($this->privates['twig'] ?? $this->getTwigService()));
    }

    /**
     * Gets the public 'link.repository' shared service.
     *
     * @return \Thunder\Linkz\Link\Infrastructure\Persistence\JsonLinkRepository
     */
    protected function getLink_RepositoryService()
    {
        return $this->services['link.repository'] = new \Thunder\Linkz\Link\Infrastructure\Persistence\JsonLinkRepository(($this->services['json.file'] ?? $this->services['json.file'] = new \Thunder\Linkz\JsonFile('../var/cache/db.json')));
    }

    /**
     * Gets the public 'router' shared service.
     *
     * @return \Symfony\Component\Routing\Router
     */
    protected function getRouterService()
    {
        return $this->services['router'] = new \Symfony\Component\Routing\Router(new \Symfony\Component\Routing\Loader\YamlFileLoader(new \Symfony\Component\Config\FileLocator(array(0 => '../etc'))), 'routing.yaml');
    }

    /**
     * Gets the private 'twig' shared service.
     *
     * @return \Twig_Environment
     */
    protected function getTwigService()
    {
        return $this->privates['twig'] = new \Twig_Environment(new \Twig_Loader_Filesystem(array(0 => '../templates')));
    }
}
