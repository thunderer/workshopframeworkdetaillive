Feature: LinkZ

  Scenario: As a visitor, when I submit a link to be shortened, I should see the shortened URL
    When I send a POST request to path /links with payload
      | field | value              |
      | [url] | http://example.com |
    Then the response code should be 302
    And the response header Location should be equal to /links/1
