<?php
declare(strict_types=1);
namespace Thunder\Linkz\Application\Linkz\Kernel;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\HttpKernelInterface;
use Symfony\Component\Routing\Exception\MethodNotAllowedException;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;
use Symfony\Component\Routing\RequestContext;

final class AgnosticKernel implements HttpKernelInterface
{
    private $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    public function handle(Request $request, $type = self::MASTER_REQUEST, $catch = true)
    {
        try {
            return $this->doHandle($request, $type, $catch);
        } catch(\Throwable $e) {
            throw $e;
            // FIXME: add logging
            return new Response(null, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function doHandle(Request $request, $type = self::MASTER_REQUEST, $catch = true)
    {
        $router = $this->container->get('router');
        $context = new RequestContext();
        $context->fromRequest($request);
        $router->setContext($context);

        try {
            $route = $router->matchRequest($request);
        } catch(ResourceNotFoundException $e) {
            return new Response(null, Response::HTTP_NOT_FOUND);
        } catch(MethodNotAllowedException $e) {
            return new Response(null, Response::HTTP_METHOD_NOT_ALLOWED);
        }

        $request->attributes->replace($route);
        [$controller, $action] = $route['controller'];


        if(false === $this->container->has($controller)) {
            return new Response(null, Response::HTTP_NOT_IMPLEMENTED);
        }
        $controller = $this->container->get($controller);

        $response = $controller->{$action}($request);

        $this->container->get('json.file')->flush();

        return $response;
    }
}
