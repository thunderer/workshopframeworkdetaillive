<?php
declare(strict_types=1);
namespace Thunder\Linkz\Application\Linkz\Context;

use Behat\Behat\Context\Context;
use Behat\Gherkin\Node\PyStringNode;
use Behat\Gherkin\Node\TableNode;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\ServerException;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7\Response;
use function GuzzleHttp\Psr7\stream_for;
use PHPUnit\Framework\Assert;
use Psr\Container\ContainerInterface;
use Ramsey\Uuid\Uuid;
use Symfony\Component\PropertyAccess\PropertyAccess;
use Thunder\Linkz\Entity\Link;
use Thunder\Linkz\Repository\JsonFile;
use Thunder\Linkz\Repository\Link\JsonLinkRepository;
use Thunder\Numbase\Numbase;

final class LinkzContext implements Context
{
    /** @var Client */
    private $client;
    /** @var ContainerInterface */
    private static $container;
    /** @var JsonFile */
    private static $json;
    /** @var array */
    private $headers = [];
    /** @var Response */
    private $response;

    public function __construct()
    {
        require_once __DIR__.'/../../var/cache/TestLinkzContainer.php';

        $this->client = new Client([
            'base_uri' => 'http://nginx',
            'allow_redirects' => false,
        ]);

        static::$container = new \TestLinkzContainer();
        static::$json = new JsonFile(__DIR__.'/../../var/cache/db.json');
    }

    /**
     * @BeforeScenario
     */
    public static function beforeScenario(): void
    {
        static::$json->reset();
        static::$json->flush();
    }

    /**
     * @Given there are following links
     */
    public function thereAreFollowingLinks(TableNode $table): void
    {
        $links = new JsonLinkRepository(static::$json, static::$container->get('numbase'));

        foreach($table->getColumnsHash() as $row) {
            $uuid = '00000000-0000-4000-0000-'.str_pad($row['id'], 12, '0', STR_PAD_LEFT);
            $links->persist(new Link(Uuid::fromString($uuid), $row['alias'], $row['url'], new \DateTimeImmutable()));
        }

        static::$json->flush();
    }

    /**
     * @Given the next request will have following headers
     */
    public function theNextRequestWillHaveFollowingHeaders(TableNode $table): void
    {
        $this->headers = $table->getColumnsHash();
    }

    /**
     * @When /^I send an? (GET|POST|OPTIONS) request to path ([^\s]+)$/
     */
    public function whenISendARequestToPath(string $method, string $path): void
    {
        $this->sendRequest(new Request($method, $path));
    }

    /**
     * @When /^I send a (GET|POST) request to path ([^\s]+) with following headers$/
     */
    public function whenISendARequestToPathWithFollowingHeaders(string $method, string $path, TableNode $headers): void
    {
        $this->sendRequest(new Request($method, $path, $headers->getRowsHash()));
    }

    /**
     * @When /^I send a (POST) request to path ([^\s]+) with payload$/
     */
    public function whenISendARequestToPathWithPayload(string $method, string $path, TableNode $payload): void
    {
        $data = [];
        $access = PropertyAccess::createPropertyAccessor();
        foreach($payload->getColumnsHash() as $row) {
            $access->setValue($data, $row['field'], $row['value']);
        }

        $this->sendRequest(new Request($method, $path, ['Content-Type' => 'application/x-www-form-urlencoded'], http_build_query($data)));
    }

    /**
     * @When /^I send a (POST) request to path ([^\s]+) with JSON payload$/
     */
    public function whenISendARequestToPathWithJsonPayload(string $method, string $path, PyStringNode $payload): void
    {
        $this->sendRequest(new Request($method, $path, [
            'body' => $payload->getRaw(),
        ]));
    }

    /**
     * @When /^I send a (GET) request to response ([^\s]+) header value$/
     */
    public function iSendRequestToResponseHeaderValue(string $method, string $header): void
    {
        $value = $this->response->getHeaderLine($header);
        echo 'string('.\strlen($value).') `'.$value.'`';
        $this->sendRequest(new Request($method, $value));
    }

    private function sendRequest(Request $request): void
    {
        foreach($this->headers as $header) {
            $request = $request->withAddedHeader($header['name'], $header['value']);
        }
        $this->headers = [];

        try {
            $this->response = $this->client->send($request);
        } catch(ClientException $e) {
            $this->response = $e->getResponse();
        } catch(ServerException $e) {
            $this->response = $e->getResponse();
        }
    }

    /**
     * @Then /^the response code should be (\d+)$/
     */
    public function theResponseCodeShouldBe(int $code): void
    {
        Assert::assertSame($code, $this->response->getStatusCode(), (string)$this->response->getBody());
    }

    /**
     * @Then /^the response should contain following dynamic content$/
     */
    public function theResponseShouldContainFollowingDynamicContent(TableNode $table): void
    {
        $access = PropertyAccess::createPropertyAccessor();
        $raw = (string)$this->response->getBody();
        $json = json_decode($raw, true);

        foreach($table->getColumnsHash() as $row) {
            if(false === $access->isReadable($json, $row['path'])) {
                throw new \RuntimeException(sprintf('Path `%s` is not readable. Response: `%s`.', $row['path'], $raw));
            }
            $value = $access->getValue($json, $row['path']);
            echo sprintf('%-10s `%-20s`:`%s`', $row['type'], $row['path'], $value)."\n";

            switch($row['type']) {
                case 'uuid': {
                    Assert::assertRegExp('/^[a-f0-9]{8}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{12}$/', $value, sprintf('Value `%s` is not an UUID', $value));
                    break;
                }
                case 'timestamp': {
                    Assert::assertInstanceOf(\DateTimeImmutable::class, new \DateTimeImmutable('@'.$value), 'Not a timestamp. Response: `'.$raw.'`.');
                    break;
                }
                default: {
                    throw new \RuntimeException(sprintf('Invalid type qualifier `%s`.', $row['type']));
                }
            }

            $access->setValue($json, $row['path'], $row['replace']);
        }

        $this->response = $this->response->withBody(stream_for(json_encode($json)));
    }

    /**
     * @Then the response should be equal JSON
     */
    public function theResponseShouldBeEqualJson(PyStringNode $json): void
    {
        $response = (string)$this->response->getBody();

        Assert::assertJsonStringEqualsJsonString($json->getRaw(), $response, $response);
    }

    /**
     * @Then the response should be empty
     */
    public function theResponseShouldBeEmpty(): void
    {
        $response = (string)$this->response->getBody();

        Assert::assertEmpty($response, $response);
    }

    /**
     * @Then the response should be equal to
     */
    public function theResponseShouldBeEqualTo(PyStringNode $payload): void
    {
        $response = (string)$this->response->getBody();

        Assert::assertSame($payload->getRaw(), $response, $response);
    }

    /**
     * @Then the response should contain
     */
    public function theResponseShouldContain(PyStringNode $payload): void
    {
        $response = (string)$this->response->getBody();

        Assert::assertContains($payload->getRaw(), $response, $response);
    }

    /**
     * @Then /^the response header ([^\s]+) should be equal to ([^$]+)$/
     */
    public function theResponseHeaderShouldBeEqualTo(string $header, string $value): void
    {
        $value = str_replace('{token}', '.*', $value);
        $value = '~^'.preg_quote($value, '~').'$~';

        Assert::assertRegExp($value, $this->response->getHeaderLine($header), $value);
    }

    /**
     * @Then the response should have following headers
     */
    public function theResponseShouldHaveFollowingHeaders(TableNode $table): void
    {
        foreach($table->getColumnsHash() as $row) {
            Assert::assertTrue($this->response->hasHeader($row['name']), 'missing header `'.$row['name'].'`');
            Assert::assertSame($row['value'], $this->response->getHeaderLine($row['name']), 'wrong value');
        }
    }
}
