<?php
declare(strict_types=1);
namespace Thunder\Linkz\Application\Linkz\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Router;

final class IndexController
{
    private $router;
    private $twig;

    public function __construct(Router $router, \Twig_Environment $twig)
    {
        $this->router = $router;
        $this->twig = $twig;
    }

    public function indexAction(Request $request): Response
    {
        return new Response($this->twig->render('index.twig', [
            'createUrl' => $this->router->generate('link.create'),
        ]));
    }
}
