<?php
declare(strict_types=1);
namespace Thunder\Linkz\Application\Linkz\Controller;

use Assert\Assertion;
use Ramsey\Uuid\Uuid;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Router;
use Thunder\Linkz\Link\Domain\Entity\Link;
use Thunder\Linkz\Link\Infrastructure\Persistence\LinkRepositoryInterface;

final class LinkController
{
    private $router;
    private $links;
    private $twig;

    public function __construct(Router $router, LinkRepositoryInterface $links, \Twig_Environment $twig)
    {
        $this->router = $router;
        $this->links = $links;
        $this->twig = $twig;
    }

    public function createAction(Request $request): Response
    {
        $url = $request->request->get('url');

        try {
            Assertion::url($url);
        } catch(\Throwable $e) {
            return new RedirectResponse($this->router->generate('index.index', ['error' => 'invalid_url']));
        }

        $alias = str_replace('-', '', Uuid::uuid4()->toString());
        $link = new Link(Uuid::uuid4(), $alias, $url, new \DateTimeImmutable());

        $this->links->persist($link);

        return new RedirectResponse($this->router->generate('link.view', [
            'alias' => $link->getAlias(),
        ]));
    }

    public function viewAction(Request $request): Response
    {
        return new Response($request->attributes->get('alias'));
    }
}
