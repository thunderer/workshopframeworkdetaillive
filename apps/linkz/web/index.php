<?php
declare(strict_types=1);
namespace X;

use Symfony\Component\HttpFoundation\Request;
use Thunder\Linkz\Application\Linkz\Kernel\AgnosticKernel;

require __DIR__.'/../../../vendor/autoload.php';
require __DIR__.'/../var/cache/LinkzContainer.php';

$request = Request::createFromGlobals();
$kernel = new AgnosticKernel(new \LinkzContainer());
$response = $kernel->handle($request);
$response->send();
