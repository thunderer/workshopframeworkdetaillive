<?php
declare(strict_types=1);
namespace Thunder\Linkz\Application\Console\Command;

use Symfony\Component\Config\FileLocator;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputDefinition;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Routing\Loader\YamlFileLoader;
use Symfony\Component\Routing\Router;

final class RoutingGenerateCommand extends Command
{
    protected function configure(): void
    {
        $this
            ->setName('routing:generate')
            ->setDescription('Regenerate container class from configuration.')
            ->setDefinition(new InputDefinition([
                new InputOption('resource', 's', InputOption::VALUE_REQUIRED, 'Path to routing YAML file'),
                new InputOption('target', 't', InputOption::VALUE_REQUIRED, 'Path to cache directory'),
            ]));
    }

    protected function execute(InputInterface $input, OutputInterface $output): void
    {
        $resource = $input->getOption('resource');
        $target = $input->getOption('target');

        $output->writeln('Generating router cache:');
        $output->writeln('  From: '.$resource);
        $output->writeln('  To: '.$target);

        $routing = new Router(new YamlFileLoader(new FileLocator([\dirname($resource)])), basename($resource), [
            'cache_dir' => $target,
        ]);
        $routing->match('/');
    }
}
