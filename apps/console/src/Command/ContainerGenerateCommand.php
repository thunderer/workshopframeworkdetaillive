<?php
declare(strict_types=1);
namespace Thunder\Linkz\Application\Console\Command;

use Symfony\Component\Config\FileLocator;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputDefinition;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Dumper\PhpDumper;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;

final class ContainerGenerateCommand extends Command
{
    protected function configure(): void
    {
        $this
            ->setName('container:generate')
            ->setDescription('Regenerate container class from configuration.')
            ->setDefinition(new InputDefinition([
                new InputOption('services', 's', InputOption::VALUE_REQUIRED, 'Path to services file'),
                new InputOption('target', 't', InputOption::VALUE_REQUIRED, 'Path to container class file'),
            ]));
    }

    protected function execute(InputInterface $input, OutputInterface $output): void
    {
        $services = $input->getOption('services');
        $target = $input->getOption('target');

        $output->writeln('Generating container class:');
        $output->writeln('  From '.$services);
        $output->writeln('  To '.$target);

        $builder = new ContainerBuilder();
        $loader = new YamlFileLoader($builder, new FileLocator(\dirname($services)));
        $loader->load(basename($services));
        $builder->compile();

        $dumper = new PhpDumper($builder);
        file_put_contents($target, $dumper->dump(['class' => str_replace('.php', '', basename($target))]));
    }
}
