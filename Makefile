install:
	docker-compose run --rm composer install

cache:
	docker-compose run --rm php sh -c "echo '{}' > apps/linkz/var/cache/db.json"
	docker-compose run --rm php chown -R www-data:www-data apps/*/var/cache
	docker-compose run --rm php chown -R www-data:www-data apps/*/var/logs
	docker-compose run --rm php apps/console/bin/linkz container:generate --services=apps/linkz/etc/services.yaml --target=apps/linkz/var/cache/LinkzContainer.php
	docker-compose run --rm php apps/console/bin/linkz container:generate --services=apps/linkz/etc/services_test.yaml --target=apps/linkz/var/cache/TestLinkzContainer.php
	docker-compose run --rm php apps/console/bin/linkz routing:generate --resource=apps/linkz/etc/routing.yaml --target=apps/linkz/var/cache
logs:
	docker-compose logs -f
clean:
	docker-compose run --rm php rm -rf apps/*/var/cache/*

docker-restart: docker-down docker-up
docker-up:
	docker-compose up -d --force-recreate
docker-down:
	docker-compose down -v --remove-orphans

test: docker-up cache test-phpunit test-behat docker-down
test-phpunit:
	docker-compose run --rm php vendor/bin/phpunit
test-behat:
	docker-compose run --rm php vendor/bin/behat
